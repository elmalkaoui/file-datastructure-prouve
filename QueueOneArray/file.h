#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED

#include "typedefs.h"
#include "stdio.h"
#include "stdlib.h"

//___________________________________________________________________
struct File
{
    value_type *obj;
    size_type frontIndex;
    size_type rearIndex;
    size_type size;
    size_type capacity;
};


typedef struct File FileA;

void file_init(FileA* s,value_type* inValues, size_type capacity);

bool file_equal(const FileA *s, const FileA *t);

size_type file_size(const FileA *s);

bool file_empty(const FileA *s);

bool file_full(const FileA *s);

value_type file_front(FileA *s);

void file_push(FileA *s, value_type v);

void file_pop(FileA *s);

#endif /* FILE_H_INCLUDED */
