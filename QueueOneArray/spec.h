#ifndef SPEC_H_INCLUDED
#define SPEC_H_INCLUDED

#include "typedefs.h"
#include "file.h"

/*@
  predicate
    EqualRanges{K,L}(value_type* a, integer n, value_type* b) =
      \forall integer i; 0 <= i < n ==> \at(a[i],K) == \at(b[i],L);

  predicate
    EqualRanges{K,L}(value_type* a, integer m, integer n, value_type* b) =
      \forall integer i; m <= i < n ==> \at(a[i],K) == \at(b[i],L);

  predicate
    EqualRanges{K,L}(value_type* a, integer m, integer n,
                     value_type* b, integer p) =
      EqualRanges{K,L}(a+m, n-m, b+p);

  predicate
    EqualRanges{K,L}(value_type* a, integer m, integer n, integer p) =
      EqualRanges{K,L}(a, m, n, a, p);

  logic integer Capacity{L}(FileA* s) = s->capacity;

  logic integer Size{L}(FileA* s) = s->size;

  logic value_type* Storage{L}(FileA* s) = s->obj;

  logic integer Top{L}(FileA* s) = s->obj[s->size-1];


  predicate
    Empty{L}(FileA* s) =  Size(s) == 0;

  predicate
    Full{L}(FileA* s)  =  Size(s) == Capacity(s);

  predicate
     Equal{S,T}(FileA* s, FileA* t) =
       Size{S}(s) == Size{T}(t) &&
       EqualRanges{S,T}(Storage{S}(s), Size{S}(s), Storage{T}(t));

  lemma
    FileAEqualReflexive{S} :
      \forall FileA* s; Equal{S,S}(s, s);

  lemma
    FileAEqualSymmetric{S,T} :
      \forall FileA *s, *t;
        Equal{S,T}(s, t) ==> Equal{T,S}(t, s);

  lemma
    FileAEqualTransitive{S,T,U}:
      \forall FileA *s, *t, *u;
        Equal{S,T}(s, t) && Equal{T,U}(t, u) ==> Equal{S,U}(s, u);

  predicate
    Invariant{L}(FileA* s) =
      0 < Capacity(s) && 0 < s->frontIndex &&
      0 <= Size(s) <= Capacity(s) &&
      \valid(Storage(s) + (0..Capacity(s)-1)) &&
      \separated(s, Storage(s) + (0..Capacity(s)-1)); 

	lemma
	FileAPushEqual{K,L}:
	  \forall FileA *s, *t;
		 Equal{K,K}(s,t)              ==>
		 Size{L}(s) == Size{K}(s) + 1 ==>
		 Size{L}(s) == Size{L}(t)     ==>
		 Top{L}(s)  == Top{L}(t)      ==>
		 EqualRanges{L,L}(Storage{L}(s), Size{K}(s), Storage{L}(t)) ==>
		 Equal{L,L}(s,t);

	predicate
	 Separated(FileA* s, FileA* t) =
	   \separated(s, s->obj + (0..s->capacity-1),
		          t, t->obj + (0..t->capacity-1));   


	predicate
		Unchanged{K,L}(value_type* a, integer m, integer n) =
		\forall integer i; m <= i < n ==> \at(a[i],K) == \at(a[i],L);

	predicate
		Unchanged{K,L}(value_type* a, integer n) =
		Unchanged{K,L}(a, 0, n);

*/

#endif /* SPEC_H_INCLUDED */

