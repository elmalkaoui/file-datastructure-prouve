
#include "typedefs.h"
#include "stdio.h"
#include "stdlib.h"
#include "spec.h"


//==========================================================================
/*@
    requires valid:     \valid(s);
    requires valid:     \valid(inValues);
    requires capacity:  0 < capacity;
    assigns             s->obj, s->capacity, s->size, s->frontIndex, s->rearIndex;
    ensures  valid:     \valid(s);
    ensures  capacity:  Capacity(s) == s->capacity;
    ensures  empty:     Size(s) ==  s->size;
*/

void file_init(FileA *s, value_type *inValues, size_type capacity)
{
    s->obj = inValues;
    s->capacity = capacity;
    s->size = 0;
    s->frontIndex = 0;
    s->rearIndex = 0;
}

//___________________________________________________________________
//___________________________________________________________________
/*@
    requires valid:     \valid(s) && Invariant(s);
    requires valid:     \valid(t) && Invariant(t);
    assigns             \nothing;
    ensures  equal:     \result == 1  <==>  Equal{Here,Here}(s, t);
    ensures  not_equal: \result == 0  <==> !Equal{Here,Here}(s, t);
*/

bool file_equal(const FileA *s, const FileA *t)
{

    if (s->size != t->size || s->capacity != t->capacity)
        return 0;

    for (int i = s->frontIndex; i < s->size; i++)
    {
        if (s->obj[i] != t->obj[i])
        {
            return 0;
        }
    }

    return 1;
}

//___________________________________________________________________
/*@
    requires valid: \valid(s) && Invariant(s);
    assigns         \nothing;
    ensures  size:  \result == Size(s);
*/
size_type file_size(const FileA *s)
{
    return s->size;
}

//___________________________________________________________________
/*@
    requires valid:    \valid(s) && Invariant(s);
    assigns            \nothing;
    ensures empty:     \result == 1  <==>  Empty(s);
    ensures not_empty: \result == 0  <==> !Empty(s);
*/
bool file_empty(const FileA *s)
{
    return s->size == 0;
}

//___________________________________________________________________
/*@
    requires valid:    \valid(s) && Invariant(s);
    assigns            \nothing;
    ensures  full:     \result == 1  <==>  Full(s);
    ensures  not_full: \result == 0  <==> !Full(s);
*/
bool file_full(const FileA *s)
{
    return s->size == s->capacity;
}

//==========================================================================
/*@
    requires valid:      \valid(s) && Invariant(s);

    behavior not_full:
        assumes            !Full(s);
        assigns            s->size;
        assigns            s->obj[s->size];
        ensures size:      Size(s) == Size{Old}(s) + 1;
        ensures capacity:  Capacity(s) == Capacity{Old}(s);
        ensures not_empty: !Empty(s);

    behavior full:
        assumes            Full(s);
        assigns            \nothing;
        ensures valid:     \valid(s) && Invariant(s);
        ensures full:      Full(s);
        ensures size:      Size(s) == Size{Old}(s);
        ensures storage:   Storage(s) == Storage{Old}(s);
        ensures capacity:  Capacity(s) == Capacity{Old}(s);
        ensures unchanged: Unchanged{Old,Here}(Storage(s), Size(s));

    complete behaviors;
    disjoint behaviors;
*/
void file_push(FileA *s, value_type v)
{

    if (!file_full(s))
    {
        s->rearIndex = (s->rearIndex + 1)%s->capacity;
        s->obj[s->rearIndex] = v;
        s->size++;
    }
    else
    {
        return ;
    }
}

//==========================================================================
/*@
    requires valid: \valid(s) && Invariant(s);

    behavior not_empty:
        assumes            !Empty(s);
        assigns            s->size;
        assigns            s->frontIndex;
        ensures size:      Size(s) == Size{Old}(s) - 1;
        ensures full:      !Full(s);
        ensures capacity:  Capacity(s) == Capacity{Old}(s);

    behavior empty:
        assumes            Empty(s);
        assigns            \nothing;
        ensures empty:     Empty(s);
        ensures size:      Size(s) == Size{Old}(s);
        ensures storage:   Storage(s) == Storage{Old}(s);
        ensures capacity:  Capacity(s) == Capacity{Old}(s);
        ensures result:    \result == -1;

    complete behaviors;
    disjoint behaviors;
*/
value_type file_front(FileA *s)
{
    if (!file_empty(s))
    {
        value_type front = s->obj[s->frontIndex];
        s->frontIndex = (s->frontIndex + 1)%s->capacity;
        s->size--;
        //s->frontIndex = file_empty(s) ? -1 : ++s->frontIndex;
        return front;
    }else 
        return -1;
}

int main(int argc, char *argv[])
{
    FileA f, f2;
    int *fvalue = malloc(sizeof(int));
    int *f2value = malloc(sizeof(int));
    file_init(&f, fvalue, 4);
    file_push(&f, 1);
    file_push(&f, 2);
    file_push(&f, 3);
    file_push(&f, 4);
    printf("front is : %d\t index = %d", file_front(&f), f.frontIndex);
    file_push(&f, 5);
    // file_init(&f2,f2value,4);
    // file_push(&f2,1);
    // file_push(&f2,2);
    // file_push(&f2,3);
    // printf("f and f2 are equals ? %s ", file_equal(&f, &f2) ? "true" : "false");
    while (!file_empty(&f))
    {
        int front = file_front(&f);
        printf("the front is : %d\t %d\n", front, f.frontIndex);
    }
    // printf("\nis the Queue empty : %d",file_empty(&f));
    return 0;
}
