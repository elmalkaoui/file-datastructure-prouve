#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED
#include "typedefs.h"
#include  "stdio.h"
#include "stdlib.h"

struct File
{
    value_type * entrants;
    value_type * sortants;
    size_type size;
    size_type size_entrants;
    size_type size_sortants;
    size_type capacity;
};

typedef struct File FileA;

void file_init(FileA *s, value_type * a, value_type * b, size_type n);

bool file_equal(const FileA *s, const FileA *t);


size_type file_size(const FileA *s);


bool file_empty(const FileA *s);

bool file_full(const FileA *s);

value_type file_front(FileA *s);

void file_push(FileA *s, value_type v);

void file_pop(FileA *s);

void copy_inverse(FileA *s);

#endif
