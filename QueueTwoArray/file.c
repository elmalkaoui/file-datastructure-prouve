#include <stdio.h>
#include <stdlib.h>
#include "spec.h"

/*@
    requires valid:     \valid(s);
    requires n:         0 < n;
    requires a:         \valid(a + (0..n-1));
    requires b:         \valid(b + (0..n-1));
    requires sep:       \separated(s, a + (0..n-1));
    assigns             s->entrants, s->sortants, s->capacity, s->size;
    ensures  valid:     \valid(s);
    ensures  capacity:  Capacity(s) == n;
    ensures  storage:   Storage(s) == a;
    ensures  invariant: Invariant(s);
    ensures  empty:     Empty(s);
*/
void file_init(FileA *s, value_type * a, value_type * b,size_type n){
    s->entrants = a;
    s->sortants = b;
    s->size = 0;
    s->size_entrants = 0;
    s->size_sortants = 0;
    s->capacity = n;
}

//___________________________________________________________________
/*@
    requires valid:     \valid(s) && Invariant(s);
    requires valid:     \valid(t) && Invariant(t);
    assigns             \nothing;
    ensures  equal:     \result == 1  <==>  Equal{Here,Here}(s, t);
    ensures  not_equal: \result == 0  <==> !Equal{Here,Here}(s, t);
*/
bool file_equal(const FileA *s, const FileA *t){
    if( s->capacity != t->capacity
            || s->size_entrants != t->size_entrants
            || s->size_sortants != t->size_sortants){
        return 0;
    }

    for(int i = 0; i< s->size_entrants;i++){
        if(s->entrants[i] != t->entrants[i]){
            return false;
        }
    }

    for(int i=0; i< s->size_sortants; i++){
        if(s->sortants[i] != t->sortants[i]){
            return false;
        }
    }

    return true;
}

//___________________________________________________________________
/*@
    requires valid: \valid(s) && Invariant(s);
    assigns         \nothing;
    ensures  size:  \result == Size(s);
*/
size_type file_size(const FileA *s){
    return s->size;
}

//___________________________________________________________________
/*@
    requires valid:    \valid(s) && Invariant(s);
    assigns            \nothing;
    ensures empty:     \result == 1  <==>  Empty(s);
    ensures not_empty: \result == 0  <==> !Empty(s);
*/
bool file_empty(const FileA *s){

    return s->size == 0;
}

//___________________________________________________________________
/*@
    requires valid:    \valid(s) && Invariant(s);
    assigns            \nothing;
    ensures  full:     \result == 1  <==>  Full(s);
    ensures  not_full: \result == 0  <==> !Full(s);
*/
bool file_full(const FileA *s){
    return s->capacity == s->size;
}

//___________________________________________________________________
void copy_inverse(FileA *s){
    for(int i=s->size_entrants; i>0 ;i--){
        s->sortants[s->size_sortants-i] = s->entrants[i];
        s->size_sortants++;
        s->size_entrants--;
    }
}

//___________________________________________________________________
/*@
    requires valid: \valid(s) && Invariant(s);
    assigns         \nothing;
    ensures  top:   !Empty(s) ==> \result == Top(s);
*/
value_type file_front(FileA *s){
 
	
    if( !file_empty(s)){
        if(s->size_sortants == 0){

            copy_inverse(s);
        }

        return s->sortants[s->size_sortants - 1];
    }
    return -1;

	
}

//___________________________________________________________________
/*@
    requires valid:      \valid(s) && Invariant(s);
    assigns              s->size, s->entrants[s->size], s->sortants[s->size];

    behavior not_full:
        assumes            !Full(s);
        assigns            s->size;
        assigns            s->entrants[s->size];
        ensures valid:     \valid(s) && Invariant(s);
        ensures size:      Size(s) == Size{Old}(s) + 1;
        ensures top:       Top(s) == Top{Old}(s);
        ensures storage:   Storage(s) == Storage{Old}(s);
        ensures capacity:  Capacity(s) == Capacity{Old}(s);
        ensures not_empty: !Empty(s);
        ensures unchanged: Unchanged{Old,Here}(Storage(s), Size{Old}(s));

    behavior full:
        assumes            Full(s);
        assigns            \nothing;
        ensures valid:     \valid(s) && Invariant(s);
        ensures full:      Full(s);
        ensures size:      Size(s) == Size{Old}(s);
        ensures storage:   Storage(s) == Storage{Old}(s);
        ensures capacity:  Capacity(s) == Capacity{Old}(s);
        ensures unchanged: Unchanged{Old,Here}(Storage(s), Size(s));

    complete behaviors;
    disjoint behaviors;
*/
void file_push(FileA *s, value_type v){

    if(file_full(s)){
        printf("%s","File pleine");
    }else{
        s->entrants[s->size_entrants++] = v;
        s->size_entrants++;
        s->size++;
    }

}
//___________________________________________________________________
/*@
    requires valid: \valid(s) && Invariant(s);
    assigns         s->size;
    ensures  valid: \valid(s) && Invariant(s);

    behavior not_empty:
        assumes            !Empty(s);
        assigns            s->size;
        ensures size:      Size(s) == Size{Old}(s) - 1;
        ensures full:      !Full(s);
        ensures storage:   Storage(s) == Storage{Old}(s);
        ensures capacity:  Capacity(s) == Capacity{Old}(s);
        ensures unchanged: Unchanged{Old,Here}(Storage(s), Size(s));

    behavior empty:
        assumes            Empty(s);
        assigns            \nothing;
        ensures empty:     Empty(s);
        ensures size:      Size(s) == Size{Old}(s);
        ensures storage:   Storage(s) == Storage{Old}(s);
        ensures capacity:  Capacity(s) == Capacity{Old}(s);
        ensures unchanged: Unchanged{Old,Here}(Storage(s), Size(s));

    complete behaviors;
    disjoint behaviors;
*/
void file_pop(FileA *s){
   if(!file_empty(s)){
       s->size--;
       s->size_sortants--;
   }

}

//___________________________________________________________________
